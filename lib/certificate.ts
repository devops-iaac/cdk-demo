import { Certificate, CertificateValidation } from 'aws-cdk-lib/aws-certificatemanager';
import { Stack, StackProps, CfnOutput } from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

interface CustomStackProps extends StackProps {
  market: string;
}
export class CertificateDemoStack extends Stack {
  constructor(scope: Construct, id: string, props?: CustomStackProps) {
    super(scope, id, props);

    // Getting external configuration values from cdk.json file
    const config = this.node.tryGetContext("markets")[props?.market||'apac'];
    
    // The code that defines your stack goes here
    const cert = new Certificate(this, 'Certificate', {
      domainName: "*."+config.domainName,
      validation: CertificateValidation.fromDns(),
    });
    
     // Certificate Arn
    new CfnOutput(this, "certificateArn", {
      value: cert.certificateArn,
      exportName: "certificateArnExport",
    });
    
    // example resource
    // const queue = new sqs.Queue(this, 'CdkDemoQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
