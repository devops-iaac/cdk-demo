import { Stack, StackProps,Fn,RemovalPolicy,CfnOutput} from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { Bucket } from "aws-cdk-lib/aws-s3";
import { BucketDeployment, Source } from "aws-cdk-lib/aws-s3-deployment";
import * as origins from "aws-cdk-lib/aws-cloudfront-origins";
import * as acm from "aws-cdk-lib/aws-certificatemanager";
import {
  OriginAccessIdentity,
  AllowedMethods,
  ViewerProtocolPolicy,
  OriginProtocolPolicy,
  Distribution,
} from "aws-cdk-lib/aws-cloudfront";

// import route53 = require("@aws-cdk/aws-route53");
// import targets = require("@aws-cdk/aws-route53-targets/lib");

// import * as sqs from 'aws-cdk-lib/aws-sqs';

interface CustomStackProps extends StackProps {
  market: string;
}

export class CloudfrontDemoStack extends Stack {
  constructor(scope: Construct, id: string, props?: CustomStackProps) {
    super(scope, id, props);

    // Importing ALB domain name
    const loadBalancerDomain = Fn.importValue("loadBalancerUrl");
      
    // Getting external configuration values from certificate stack
    const certificateArnImport = Fn.importValue("certificateArnExport");


    // SSL certificate 
    const certificateArn = acm.Certificate.fromCertificateArn(this, "tlsCertificate", certificateArnImport);

    // Web hosting bucket
    let websiteBucket = new Bucket(this, "websiteBucket", {
      versioned: false,
      publicReadAccess: true,
      removalPolicy: RemovalPolicy.DESTROY,
    });

    // Trigger frontend deployment
    new BucketDeployment(this, "websiteDeployment", {
      sources: [Source.asset("./application/frontend/build")],
      destinationBucket: websiteBucket as any
    });

    // Create Origin Access Identity for CloudFront
    const originAccessIdentity = new OriginAccessIdentity(this, "cloudfrontOAI", {
      comment: "OAI for web application cloudfront distribution",
    });
    
    // Getting external configuration values from cdk.json file
    const config = this.node.tryGetContext("markets")[props?.market||'apac'];

    // Creating CloudFront distribution
    let cloudFrontDist = new Distribution(this, "cloudfrontDist", {
      defaultRootObject: "index.html",
      domainNames: [config.subDomain+"."+config.domainName],
      certificate: certificateArn,
      defaultBehavior: {
        origin: new origins.S3Origin(websiteBucket as any, {
          originAccessIdentity: originAccessIdentity as any,
        }) as any,
        compress: true,
        allowedMethods: AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
        viewerProtocolPolicy: ViewerProtocolPolicy.HTTPS_ONLY,
      },
    });

    // Creating custom origin for the application load balancer
    const loadBalancerOrigin = new origins.HttpOrigin(loadBalancerDomain, {
      protocolPolicy: OriginProtocolPolicy.HTTP_ONLY,
    });

    // Creating the path pattern to direct to the load balancer origin
    cloudFrontDist.addBehavior("/superheroes/identify/*", loadBalancerOrigin as any, {
      compress: true,
      viewerProtocolPolicy: ViewerProtocolPolicy.ALLOW_ALL,
      allowedMethods: AllowedMethods.ALLOW_ALL,
    });
    
    // // Route53 alias record for the CloudFront distribution
    // const zone = route53.HostedZone.fromLookup(this, "Zone", {
    //   domainName: config.domainName,
    // })
    // const siteDomain = config.siteSubDomain + "." + config.domainName;
    // new route53.ARecord(this, "SiteAliasRecord", {
    //   recordName: siteDomain,
    //   target: route53.RecordTarget.fromAlias(
    //     new targets.CloudFrontTarget(cloudFrontDist)
    //   ),
    //   zone,
    // })

    // Cloudfront url output
    new CfnOutput(this, "cloudfrontDomainUrl", {
      value: cloudFrontDist.distributionDomainName,
      exportName: "cloudfrontDomainUrl",
    });
  }
}
