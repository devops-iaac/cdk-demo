import { Stack, StackProps,Fn,CfnOutput} from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { Vpc } from "aws-cdk-lib/aws-ec2";
import { Cluster, ContainerImage } from "aws-cdk-lib/aws-ecs";
import { ApplicationLoadBalancedFargateService } from "aws-cdk-lib/aws-ecs-patterns";

// import * as sqs from 'aws-cdk-lib/aws-sqs';

interface CustomStackProps extends StackProps {
  market: string;
}
export class FargateDemoStack extends Stack {
  constructor(scope: Construct, id: string, props?: CustomStackProps) {
    super(scope, id, props);

     // VPC
    const vpc = new Vpc(this, "fargateVPC", {
      maxAzs: 2,
      natGateways: 1,
    });

    // Fargate cluster
    const cluster = new Cluster(this, "fargateCluster", {
      vpc: vpc as any,
    });

    // Getting external configuration values from cdk.json file
    const config = this.node.tryGetContext("markets")[props?.market||'apac'];

    // Fargate service
    const backendService = new ApplicationLoadBalancedFargateService(this, "backendService", {
      cluster: cluster,
      memoryLimitMiB: 1024,
      cpu: 512,
      desiredCount: 2,
      taskImageOptions: {
        image: ContainerImage.fromAsset("./application/backend/"),
        environment: {
          origin: "https://"+config.subDomain+"."+config.domainName,
        },
      },
    });

    // Health check
    backendService.targetGroup.configureHealthCheck({ path: "/health" });

    // Load balancer url
    new CfnOutput(this, "loadBalancerUrl", {
      value: backendService.loadBalancer.loadBalancerDnsName,
      exportName: "loadBalancerUrl",
    });
    
  }
}
