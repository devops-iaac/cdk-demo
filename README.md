# Welcome to your CDK TypeScript project

This is a blank project for CDK development with TypeScript.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template

===============

npm install -g aws-cdk

cdk --version

cdk init --language typescript

cdk init cannot be done in a non-empty folder


**Initialization**

After cloning the run , run `git submodule update --init --recursive --merge`
If on Windows, correct the EOL conversion of entrypoint.sh


npx aws-cdk deploy YOUR-STACK-NAME --outputs-file ./cdk-outputs.json