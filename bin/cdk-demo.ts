#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { FargateDemoStack } from "../lib/fargate";
import { CloudfrontDemoStack } from "../lib/cloudfront";
import { CertificateDemoStack } from "../lib/certificate";

const app = new cdk.App();
const deploymentMarket="emea";
// Cloudfront stack
new CloudfrontDemoStack(app, "CloudfrontDemoStack", {
  market: deploymentMarket,
  env: { account: "465392140585", region: "us-east-1" },
});

// Fargate stack
new FargateDemoStack(app, "FargateDemoStack", {
  market: deploymentMarket,
  env: { account: "465392140585", region: "us-east-1" },
});

// Certificate stack
new CertificateDemoStack(app, "CertificateDemoStack", {
  market: deploymentMarket,
  env: { account: "465392140585", region: "us-east-1" },
});